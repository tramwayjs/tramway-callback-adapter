const assert = require('assert');
const utils = require('tramway-core-testsuite');
const lib = require('../index.js');
var describeCoreClass = utils.describeCoreClass;
var describeFunction = utils.describeFunction;

describe("Simple acceptance tests to ensure library returns what's promised.", function(){
    console.log(lib)
    describe("Should return a proper 'Command' class", describeCoreClass(
        lib.default, 
        "CallbackAdapter", 
        ['executeAsyncAsCallback'],
        [],
        function(testClass, testInstance, classFunctions, instanceFunctions) {
            describe("The 'executeAsyncAsCallback' function should have the same signature", describeFunction(
                testClass["executeAsyncAsCallback"], 
                ["cb", "func", "...args"]
            ));
        }
    ));
});