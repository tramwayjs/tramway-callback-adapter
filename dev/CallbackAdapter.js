export default class CallbackAdapter {
    /**
     * @async
     * @param {function} cb Callback
     * @param {AsyncFunction} func Async function
     * @param {...any} ...args Arguments for Async function
     */
    static async executeAsyncAsCallback(cb, func, ...args) {
        try {
            let a = await func(...args);
            return cb(null, a)
        } catch (e) {
            return cb(e)
        }
    }
}